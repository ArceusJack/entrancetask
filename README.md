## **Entrance Tasks**

Expected Deadline: 2019/8/31 23:59. 

How to submit: ***Fork*** this repository and ***commit*** to your own forked repository, and make ***pull request*** to make change to this repository. We will review your codes in pull request. 

Feel free to ask Shenlai GAO(sgaoae@gmail.com) if you have any questions. 

---

### **Task 1.** ***Queue*** Implentation using ***Typescript***
Description: implement a simple ***Queue*** using ***Typescript***, and test it with ***jest***, which is a Javascript testing tool. It is better if your implentation is very efficient. 

There is a template at **Queue.ts**

#### Task 1.1. Implement it with an Interface
Implement the class Queue with ***Interface*** IQueue. [Typescript Tutorial](https://ts.xcatliu.com/advanced/class-and-interfaces). 

#### Task 1.2. Test your implentation
Test it with jest. Feel free to write your test cases to have a thorough test, if time permits. [jest Documentation](https://jestjs.io/docs/en/getting-started)

---

### **Task 2.** Level-of-detail Map Downloader
Description: read [Bing Maps Tile System](https://docs.microsoft.com/en-us/bingmaps/articles/bing-maps-tile-system) to understand quadtree map structures. 

you will get an url template with level, x, y as the parameters, and the url template may be given as a function. You will need to implement a downloader to download these maps. 

url template examples:
```javascript
// not a real one, just used for explanation. 
// level: integer, x: integer, y: integer
function mapUrl(level, x, y) {
  return 'http://someimage.xxx/maps/' + level + '/' + x + '/' + y + '.png'
}
// image with level = 0, x = 0, y = 0 (the image at root) will have url = mapUrl(0, 0, 0) = 'http://someimage.xxx/maps/0/0/0.png'
// image with level = 4, x = 7, y = 9 will have url = mapUrl(4, 7, 9) = 'http://someimage.xxx/maps/4/7/9.png'
// normally, 0 <= x, y < Math.pow(2, level)
```
```javascript
//-------------------------------------------------------------------
// the codes below are only used to make explanations more clear,
// you can also implement a class and make these functions as its methods, other implentations are also totally ok. 
// feel free to do our own ways of implentation

// a given url template for download map from google
function googleMapUrl(level, x, y) {
  return 'http://mt2.google.cn/vt/lyrs=m@167000000&hl=zh-CN&gl=cn&x='+ x +'&y=' + y + '&z=' + level
}
let URLTemplate = googleMapUrl
// Task 2.1
function downloadMapAt(level, x, y) {
  let url = URLTemplate(level, x, y)
  return someDownloadHelper(url) // this can be some download tools such as axios or XMLHTTPRequest
}
// Task 2.2
function donwloadMapsAsRoot(level, x, y) {
  let rootUrl = URLTemplate(level, x, y)
  let childrenLevel = ...
  let childrenX = ...
  let childrenY = ...
  let childrenUrl = URLTemplate(childrenLevel, childrenX, childrenY)
  // here we just send the request after urls are made
  // you can improve this part in Task 2.3 and Task 2.4
  someDownloadHelper(rootUrl)
  someDownloadHelper(childrenUrl)
  ...
}
```

#### Task 2.1. Download a map at (level, x, y). 
A map with custom (level, x, y) should be downloaded by your downloader. 

#### Task 2.2. Start from a root with (level, x, y), download maps whose level < (root's level + depth). 
For a map (level, x, y), its four children will be 
(level + 1, 2 * x, 2 * y), (level + 1, 2 * x + 1, 2 * y), (level + 1, 2 * x, 2 * y + 1), (level + 1, 2 * x + 1, 2 * y + 1). 

All maps with level < (root's level + depth) which are decendents of map with (level, x, y) should be downloaded. 

#### Task 2.3. Control the map downloading scheduel. 
The implentation of ***throttle*** and ***debounce*** are provided in **Downloader.js**

Add ***throttle*** or ***debounce*** to control the downloading process, like fire a download request every 2 seconds. (Users can also specify a custom time as well.) You may also add some ***randomness***. (this task is similar to writing a python crawler, you will need to download the required maps without overloading the server, and not to be regarded as robot and get banned.)

#### Extra Task 2.4. Concurrency. 

This is more like a bonus task with a little difficulty. There should be only one download request at any time. A new request will be fired when and as soon as result of last request is returned. 

---

### **Task 3.** Implement a ***Skybox*** using ***THREE.JS***

Description: you will need to implement a skybox with a sphere (as earth) centered in the skybox. Use ***three@0.97.0*** (already specified in **package.json**). 

Some useful skeletons are provided in **SkyBox.js** and **SkyTexture.js**

And an example is provided in **examples/skybox.html** without packing the code

The sphere should be of radius **RADIUS** specified in the skeleton. 

this github repository may be helpful for your implentation:
https://github.com/wwwtyro/space-3d

#### Task 3.1. Texture Generation
Instead of downloading maps from the Internet, all the textures should be generated. 

#### Task 3.2. Pack the codes
Use [***yarn***](https://yarnpkg.com/zh-Hant/) to manage package installation and use [***node***](https://nodejs.org/en/) version 10.15.3 

To install and manage the node versions, you may find [***nvm***](https://github.com/nvm-sh/nvm) helpful. 

You will need to use [***babel***](https://babeljs.io/) and [***webpack 4***](https://webpack.js.org/) to pack your codes, so that it works under IE 11.

You will need to:
1. install **node** 10.15.3
2. install packages by **yarn**
3. write **webpack.config.js** to setup webpack 4 and **.babelrc** to setup babel

**package.json** (project configuration file by yarn), **tsconfig.json** (required by webpack 4 to pack Typescript file) are provided. 

You may find following command useful:
```bash
# to install packages, execute this in repository after node is installed
yarn
# see package.json, you will see it is running release under scripts
# you can also write your own script
# this pack the codes
yarn release
# check node version
node -v
# setup a python3 simple http server on port 8000
python -m http.server 8000
# or
# python3 -m http.server 8000
```
