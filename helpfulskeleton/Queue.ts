/**
 * @fileoverview A simple queue data structure.
 * @author Jingbo<jingbo@altizure.com>
 */

interface IQueue<T> {
  getLength: () => number,
  enqueue: (item: T) => void,
  dequeue: () => T | undefined,
  isEmpty: () => boolean,
  peek: () => T | undefined,
  clear: () => void
}

/**
 * A first in first out queue.
 *
 * @class Queue
 */
class Queue<T> implements IQueue<T>{
  constructor () {}
  getLength ():number {
    // TODO
    return 0
  }
  enqueue (item: T): void {
    // TODO
  }
  dequeue (): T | undefined {
    // TODO
    return undefined
  }
  isEmpty (): boolean {
    // TODO  
    return true
  }
  peek (): T | undefined{
    // TODO
    return undefined
  }
  clear (): void {
    // TODO
  }
}

export default Queue
