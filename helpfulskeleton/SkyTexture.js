import {
  // WebGLRenderer,
  WebGLRenderTarget,
  Scene, PerspectiveCamera,
  LinearFilter, RGBAFormat,
  // NearestFilter,
  Vector3, Color, PointsMaterial,
  BufferGeometry, BufferAttribute, VertexColors,
  // SphereBufferGeometry, Mesh, MeshBasicMaterial, DoubleSide,
  Points
} from 'three'

// import { MAJOR_AXIS } from '../../../Math/PlanetConstants'

const RESOLUTION = 512
var bufferTarget = new WebGLRenderTarget(RESOLUTION, RESOLUTION,
  {
    minFilter: LinearFilter,
    magFilter: LinearFilter,
    format: RGBAFormat
  })
bufferTarget.setSize(RESOLUTION, RESOLUTION)

var scene = new Scene()
// renderer = window.__renderer
// renderer.clear()
// var renderer = window.__renderer // new WebGLRenderer()
// renderer.setSize(RESOLUTION, RESOLUTION)

var camera = new PerspectiveCamera(40, 1, 1, 10000)

function pointStarTexture ({
  NSTARS, // = 10000,
  radius, // = 200,
  baseColor, // = { h: 0.5, s: 0.7, l: 0.5 },
  transparent, // = false,
  opacity // = 1
} = {}) {
  let positions = new Float32Array(3 * NSTARS)
  let colors = new Float32Array(3 * NSTARS)

  let vertex = new Vector3()
  let color = new Color(0xffffff)
  for (let i = 0; i < NSTARS; i++) {
    vertex.x = (Math.random() * 2 - 1) * radius
    vertex.y = (Math.random() * 2 - 1) * radius
    vertex.z = (Math.random() * 2 - 1) * radius
    vertex.toArray(positions, i * 3)

    color.setHSL(baseColor.h + 0.2 * (i / NSTARS), baseColor.s, baseColor.l)
    color.toArray(colors, i * 3)
  }

  let geometry = new BufferGeometry()
  geometry.addAttribute('position', new BufferAttribute(positions, 3))
  geometry.addAttribute('color', new BufferAttribute(colors, 3))
  var starsMaterial = new PointsMaterial({ vertexColors: VertexColors, size: 1, sizeAttenuation: false })
  let pts = new Points(geometry, starsMaterial)
  scene.add(pts)

  let renderer = window.__renderer
  // keep old states
  // let originalClearColor = renderer.getClearColor()
  // let originalClearAlpha = renderer.getClearAlpha()

  // renderer.setClearColor(new Color(0x030303), (transparent) ? opacity : 1)
  renderer.clearColor(0, 0, 0, (transparent) ? opacity : 1)
  // magic code: https://stackoverflow.com/questions/38583219/three-js-render-to-texture
  renderer.setRenderTarget(bufferTarget)
  renderer.render(scene, camera, bufferTarget, false)
  renderer.setRenderTarget(null)

  // restore states
  // renderer.setClearColor(originalClearColor)
  // renderer.setClearAlpha(originalClearAlpha)

  let tex = bufferTarget.texture
  if (window && window.viewer && window.viewer._displayTexture) {
    window.viewer._displayTexture(tex)
  }

  scene.remove(pts)
  // scene.dispose()
  return tex
}

export { pointStarTexture }
